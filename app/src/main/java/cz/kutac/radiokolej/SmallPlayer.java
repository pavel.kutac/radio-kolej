package cz.kutac.radiokolej;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RemoteViews;


/**
 * Implementation of App Widget functionality.
 */
public class SmallPlayer extends AppWidgetProvider {

    private String action = "pause";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        this.updateAppWidget(context,appWidgetManager);
        //final int N = appWidgetIds.length;
        /*for (int i = 0; i < N; i++) {

            int appWidgetId = appWidgetIds[i];





            //appWidgetManager.updateAppWidget(appWidgetId,views);

        }*/

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        //Log.d("RK","onReceive widget");
        if(intent.hasExtra(RKNaming.INTENT_NAME)){
            String type = intent.getStringExtra(RKNaming.KEY_TYPE);
            if(RKNaming.TYPE_EVENT.equals(type)){
                action = intent.getStringExtra(RKNaming.KEY_EVENT);

                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                this.updateAppWidget(context,appWidgetManager);


                //appWidgetManager.updateAppWidget(new ComponentName(context,SmallPlayer.class),views);*/

            }/*else if(RKNaming.TYPE_UPDATE.equals(type)){
                setCurrentPlaying(intent.getStringExtra(RKNaming.ARTIST),intent.getStringExtra(RKNaming.TRACK),intent.getStringExtra(RKNaming.SHOW));
                //Log.d("RK","Update " + intent.getStringExtra("artist") + " - " + intent.getStringExtra("title") + " @ " + intent.getStringExtra("show"));
            }*/
        }
    }

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager) {

        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.small_player);

        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra(RKNaming.INTENT_NAME,"");
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);

        views.setOnClickPendingIntent(R.id.widget_play_button,pendingIntent);

        if(RKNaming.EVENT_LOADING.equals(action)){
            views.setImageViewResource(R.id.widget_play_button, R.drawable.loading_static);
        }else if(RKNaming.EVENT_PLAY.equals(action)){
            views.setImageViewResource(R.id.widget_play_button, R.drawable.pause_button);
        }else{
            views.setImageViewResource(R.id.widget_play_button, R.drawable.play_button);
        }
        appWidgetManager.updateAppWidget(new ComponentName(context,SmallPlayer.class),views);

    }
}


