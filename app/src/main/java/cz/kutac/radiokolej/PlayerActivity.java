package cz.kutac.radiokolej;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class PlayerActivity extends AppCompatActivity implements View.OnClickListener {

    private PlayerService playerService;
    private Intent playerIntent;

    //private Button playBut, stopBut;
    private ImageView playButton;
    private TextView artistText, trackText, showText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*if( ! AndroidUtils.isOnline(this)){
            this.finish();
            return;
        }*/

        getSupportActionBar().hide();

        setContentView(R.layout.activity_player);

        playButton = (ImageView)findViewById(R.id.play_button);
        playButton.setOnClickListener(this);

        artistText = (TextView)findViewById(R.id.textArtist);
        trackText = (TextView)findViewById(R.id.textTrack);
        showText = (TextView)findViewById(R.id.textShow);

        artistText.setSelected(true);
        trackText.setSelected(true);
        showText.setSelected(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_player, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*switch(item.getItemId()){
            case R.id.action_settings:
                Intent i 
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(playerIntent == null){
            playerIntent = new Intent(this,PlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, playerConnection, Context.BIND_AUTO_CREATE);
            Log.d("RK","PlayerActivity.java - bind service");
        }

        registerReceiver(rkMessageReceiver,new IntentFilter(RKNaming.INTENT_NAME));
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(playerService != null) {

            if (!playerService.isPlaying()) {
                playerService.stopService(new Intent(this, PlayerService.class));
                Log.d("RK", "PlayerActivity.java - stop service");
            } else {
                this.unbindService(playerConnection);
                Log.d("RK", "PlayerActivity.java - unbind service");
            }
        }

        unregisterReceiver(rkMessageReceiver);

        playerIntent = null;
    }

    @Override
    public void onClick(View v) {
        if(playerService != null){
            playerService.pushButton();
        }else{
            Toast.makeText(this,R.string.toast_deadService,Toast.LENGTH_SHORT).show();
            this.finish();
        }
        /*if(v.getId() == playBut.getId()){
            if(playerService != null){
                playerService.playRadio();
            }
        }else if(v.getId() == stopBut.getId()){
            if(playerService != null){
                playerService.stopRadio();
            }
        }*/
    }

    private ServiceConnection playerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayerService.PlayerBinder binder = (PlayerService.PlayerBinder)service;
            playerService = binder.getService();

            if(playerService.isLoading()){
                setCurrentButtonState(RKNaming.EVENT_LOADING);
            }else{
                setCurrentButtonState( playerService.isPlaying() ? RKNaming.EVENT_PLAY : RKNaming.EVENT_STOP);
            }

            setCurrentPlaying(playerService.getArtist(),playerService.getTrack(),playerService.getShow());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            playerService = null;
        }
    };

    protected void setCurrentButtonState(String state){
        playButton.setAnimation(null);
        if(RKNaming.EVENT_LOADING.equals(state)){
            playButton.setImageResource(R.drawable.loading);
            RotateAnimation animation = new RotateAnimation(0f,359f,Animation.RELATIVE_TO_SELF ,0.5f,Animation.RELATIVE_TO_SELF, 0.5f);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setDuration(1500);

            playButton.startAnimation(animation);

        }else if(RKNaming.EVENT_PLAY.equals(state)){
            playButton.setImageResource(R.drawable.pause_button);
        }else{
            playButton.setImageResource(R.drawable.play_button);
        }
        if(RKNaming.EVENT_INTERNET_STOPS.equals(state)){
            Toast.makeText(this,R.string.toast_internetMissing,Toast.LENGTH_LONG).show();
        }
    }

    protected void setCurrentPlaying(String artist, String track, String show){
        if (! artistText.getText().equals(artist)) {
            artistText.setText(artist);
        }
        if(! trackText.getText().equals(track)) {
            trackText.setText(track);
        }
        if(! showText.getText().equals(show)) {
            showText.setText(show);
        }
    }


    private BroadcastReceiver rkMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra(RKNaming.KEY_TYPE);
            if(RKNaming.TYPE_EVENT.equals(type)){
                String action = intent.getStringExtra(RKNaming.KEY_EVENT);
                setCurrentButtonState(action);
                Log.d("RK","PlayerActivity.java - Event message " + action);
            }else if(RKNaming.TYPE_UPDATE.equals(type)){
                setCurrentPlaying(intent.getStringExtra(RKNaming.ARTIST),intent.getStringExtra(RKNaming.TRACK),intent.getStringExtra(RKNaming.SHOW));
                //Log.d("RK","Update " + intent.getStringExtra("artist") + " - " + intent.getStringExtra("title") + " @ " + intent.getStringExtra("show"));
            }
        }
    };

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
