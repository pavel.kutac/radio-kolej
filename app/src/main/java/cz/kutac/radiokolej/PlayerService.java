package cz.kutac.radiokolej;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class PlayerService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener {

    private MediaPlayer player;

    private final IBinder playerBinder = new PlayerBinder();
    private boolean loading = false;
    private String show, artist, track;

    private Intent homeScreen;
    private PendingIntent notifIntent;
    private NotificationCompat.Builder builder;
    private NotificationManager notifyManager;
    private RKDownloader downloader;
    private boolean playingInterupted = false;

    Timer timer;

    AudioManager audioManager;

    public PlayerService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("RK", "PlayerService.java - Service onCreate");

        show = getResources().getString(R.string.defaultShow);
        artist = getResources().getString(R.string.defaultArtist);
        track = getResources().getString(R.string.defaultTrack);

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Log.d("RK", "PlayerService.java - Timer run");
                downloader = new RKDownloader(PlayerService.this);
                downloader.execute();

            }
        }, 0, 15000);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(internetConnectionChange, intentFilter);

        try {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }catch (Exception e){
            Log.e("RK","Cant receive audio service - Exception"/*,e*/);
        }
        homeScreen = new Intent(this,PlayerActivity.class);
        homeScreen.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        homeScreen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        notifIntent = PendingIntent.getActivity(this,0,homeScreen,PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap notifyIconLarge = BitmapFactory.decodeResource(getResources(),R.drawable.notify_large);
        Bitmap notifyIconLargeScaled = notifyIconLarge;


        if(Build.VERSION.SDK_INT >= 11) {
            int height = (int) getResources().getDimension(android.R.dimen.notification_large_icon_height);
            int width = (int) getResources().getDimension(android.R.dimen.notification_large_icon_width);
            notifyIconLargeScaled = Bitmap.createScaledBitmap(notifyIconLarge,width,height,false);
        }

        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notify_small)
                .setLargeIcon(notifyIconLargeScaled)
                .setContentTitle(getResources().getString(R.string.notifyTitle))
                .setAutoCancel(false)
                .setOngoing(true);
        builder.setContentIntent(notifIntent);

        notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int ret = super.onStartCommand(intent, flags, startId);
        Log.d("RK","PlayerService.java - Service onStartCommand");

        if(intent.hasExtra(RKNaming.INTENT_NAME)){
            this.pushButton();
            if(!isPlaying()){
                this.stopSelfService();
            }
        }


        return ret;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("RK","PlayerService.java - Service onBind");
        return playerBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        boolean ret = super.onUnbind(intent);
        Log.d("RK", "PlayerService.java - Service onUnbind");
        if(!isPlaying()){
            this.stopSelfService();
        }

        return ret;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideNotification();
        if(player != null){
            player.stop();
            player.release();
        }
        if(timer != null){
            timer.cancel();
            timer = null;
        }
        try {
            unregisterReceiver(internetConnectionChange);
        }catch (Exception e){
            Log.d("RK","PlayerService.java - Internet connection can't be unregistered");
        }
        Log.d("RK", "PlayerService.java - onDestroy");
    }

    public void stopSelfService(){
        if(downloader != null){
            try {
                downloader.cancel(true);
            }catch (Exception e){
                Log.d("RK","PlayerService.java - Downloader not available" + e.getMessage());
            }
        }
        Log.d("RK","PlayerService.java - self stop");
        stopSelf();
    }

    private void sendRKMessage(String type, MessageEntry... values)
    {
        Intent intent = new Intent(RKNaming.INTENT_NAME);
        intent.putExtra(RKNaming.INTENT_NAME, "");
        intent.putExtra(RKNaming.KEY_TYPE, type);

        for(MessageEntry entry : values){
            intent.putExtra(entry.getName(), entry.getValue());
        }

        if(RKNaming.TYPE_UPDATE.equals(type)){
            if(isLoading()) {
                intent.putExtra(RKNaming.KEY_EVENT, RKNaming.EVENT_LOADING);
            }else{
                intent.putExtra(RKNaming.KEY_EVENT, isPlaying() ? RKNaming.EVENT_PLAY : RKNaming.EVENT_STOP);
            }
        }else if(RKNaming.TYPE_EVENT.equals(type)){
            intent.putExtra(RKNaming.ARTIST, artist);
            intent.putExtra(RKNaming.TRACK, track);
            intent.putExtra(RKNaming.SHOW, show);
        }

        sendBroadcast(intent);
    }

    private void initPlayer()
    {
        player = new MediaPlayer();

        player.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
        player.setOnErrorListener(this);
    }

    public void pushButton(){
        if(isPlaying()){
            stopRadio();
        }else{
            playRadio();
        }

    }


    public void playRadio(){
        if(isPlaying()){
            stopRadio();
        }
        if (!AndroidUtils.isOnline(this)){
            Toast.makeText(this, R.string.toast_internetMissing, Toast.LENGTH_SHORT).show();
            return;
        }
        try {

            int result = audioManager.requestAudioFocus(this,AudioManager.STREAM_MUSIC,AudioManager.AUDIOFOCUS_GAIN);

            if(result == AudioManager.AUDIOFOCUS_REQUEST_FAILED){
                sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT, RKNaming.EVENT_REQUEST_FAIL));
                Toast.makeText(this, R.string.toast_noAudioFocus, Toast.LENGTH_SHORT).show();
                return;
            }

            registerReceiver(headphonesUnplug, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT, RKNaming.EVENT_LOADING));
            if(player == null){
                initPlayer();
            }
            player.setDataSource(this, Uri.parse("http://stream.radiokolej.cz:8000/radio-kolej-128.mp3"));
            player.prepareAsync();
            loading = true;
            showNotification();
            playingInterupted = false;
        }catch (Exception e){
            sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT,RKNaming.EVENT_ERROR));
            Toast.makeText(this, R.string.toast_streameMissing, Toast.LENGTH_SHORT).show();
            Log.e("RK","Stream failed - exception"/*,e*/);
            stopRadio();
            this.stopSelfService();
        }
    }

    public void stopRadio(){
        try {
            loading = false;
            hideNotification();
            if(player != null) {
                player.stop();
                player.reset();
                sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT,RKNaming.EVENT_STOP));
            }
            if(audioManager != null){
                audioManager.abandonAudioFocus(this);
            }
            try {
                unregisterReceiver(headphonesUnplug);
            }catch (Exception e){
                Log.d("RK","PlayerService.java - unregistered");
            }

        }catch (Exception e){
            Log.e("RK","PlayerService.java - Stop failed",e);
        }
    }

    public String getArtist() {
        return artist;
    }

    public String getTrack() {
        return track;
    }

    public String getShow() {
        return show;
    }

    public boolean isLoading(){
        if(player != null && player.isPlaying()){
            return false;
        }
        return loading;
    }


    public boolean isPlaying(){
        if(player == null){
            return false;
        }
        return player.isPlaying() || loading;
    }

    private void showNotification()
    {
        if(!isPlaying() ){
            return;
        }
        builder.setContentText(artist + " - " + track);
        builder.setContentTitle(getResources().getString(R.string.notifyTitle) + " - " + show);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR;

        notifyManager.notify(0, builder.build());
    }

    private void hideNotification(){
        notifyManager.cancelAll();
    }

    public void receiveCurrentData(JSONObject json){
        if(json == null){
            return;
        }
        try {
            artist = json.getString(RKNaming.ARTIST);
            show = json.getString(RKNaming.SHOW);
            track = json.getString(RKNaming.TRACK);

            Log.d("RK","PlayerService.java - receiveData");

            sendRKMessage(RKNaming.TYPE_UPDATE, new MessageEntry(RKNaming.ARTIST, artist), new MessageEntry(RKNaming.TRACK, track), new MessageEntry(RKNaming.SHOW, show));
            showNotification();
        }catch (Exception e){
            Log.e("RK","JSON object error - exception"/*,e*/);
        }
    }



    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT, RKNaming.EVENT_ERROR));
        Log.d("RK", "PlayerService.java - MediaPlayer onError");
        Toast.makeText(this, R.string.toast_internetMissing, Toast.LENGTH_SHORT).show();
        stopRadio();
        player = null;
        this.stopSelfService();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        try {
            sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT,RKNaming.EVENT_PLAY));
            mp.start();
            loading = false;
        }catch (Exception e){
            Log.e("RK","Player fail - exception"/*,e*/);
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.d("RK", String.format("PlayerService.java - Focus change %d", focusChange));
        if(focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT){
            stopRadio();
            Log.d("RK","PlayerService.java - Loss transient");
        }else if(focusChange == AudioManager.AUDIOFOCUS_GAIN){
            playRadio();
            Log.d("RK","PlayerService.java - Focus gain");
        }else/* if(focusChange == AudioManager.AUDIOFOCUS_LOSS)*/{
            stopRadio();
            this.stopSelfService();
            Log.d("RK","PlayerService.java - Focus loss");
        }
    }

    public class PlayerBinder extends Binder{
        PlayerService getService()
        {
            return PlayerService.this;
        }
    }

    private BroadcastReceiver headphonesUnplug = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                stopRadio();
            }
        }
    };

    private BroadcastReceiver internetConnectionChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isOnline = AndroidUtils.isOnline(context);
            Log.d("RK","PlayerService.java - internetConnectionChange: online =  " + Boolean.toString(isOnline) + ", interupted: " + Boolean.toString(playingInterupted));
            if (isOnline){
                if(playingInterupted){
                    playRadio();
                }
            }else{
                if (isPlaying() || isLoading()){
                    playingInterupted = true;
                    stopRadio();
                    sendRKMessage(RKNaming.TYPE_EVENT, new MessageEntry(RKNaming.KEY_EVENT, RKNaming.EVENT_INTERNET_STOPS));
                }
            }
        }
    };


}
