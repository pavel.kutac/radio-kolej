package cz.kutac.radiokolej;

/**
 * Created by PavelK on 13. 12. 2014.
 */
public class MessageEntry {
    private final String name, value;

    public MessageEntry(String name, String value){
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
