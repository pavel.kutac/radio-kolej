package cz.kutac.radiokolej;

/**
 * Created by PavelK on 14. 12. 2014.
 */
public class RKNaming {

    static public String INTENT_NAME = "RK_event";

    static public String KEY_TYPE = "type";


    static public String KEY_EVENT = "name";

    static public String TYPE_EVENT = "event";
    static public String TYPE_UPDATE = "update";

    static public String EVENT_PLAY = "play";
    static public String EVENT_STOP = "stop";
    static public String EVENT_LOADING = "loading";
    static public String EVENT_ERROR = "error";
    static public String EVENT_REQUEST_FAIL = "request-fail";
    static public String EVENT_INTERNET_STOPS = "internet-stops";

    static public String ARTIST = "artist";
    static public String TRACK = "title";
    static public String SHOW = "show";
    static public String PLAYING_URL = "http://www.radiokolej.cz/mobileApp/androidPlaying.php";
    static public String STREAMS_URL = "http://www.radiokolej.cz/mobileApp/androidStreams.php";
}
