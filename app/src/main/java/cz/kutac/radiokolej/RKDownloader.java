package cz.kutac.radiokolej;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by PavelK on 13. 12. 2014.
 */
public class RKDownloader extends AsyncTask<String, Integer, Integer> {

    StringBuilder result = new StringBuilder();
    boolean downloadSettings;
    PlayerService playerService;

    DefaultHttpClient httpClient;
    HttpPost httpPost;

    public RKDownloader(PlayerService playerService){
        this(playerService,false);
    }

    public RKDownloader(PlayerService playerService, boolean downloadSettings){
        this.playerService = playerService;
        this.downloadSettings = downloadSettings;
    }

    @Override
    protected Integer doInBackground(String... params) {
        try {
            httpClient = new DefaultHttpClient(new BasicHttpParams());
            if(downloadSettings){
               httpPost = new HttpPost(RKNaming.STREAMS_URL);
            } else{
                httpPost = new HttpPost(RKNaming.PLAYING_URL);
            }
            httpPost.setHeader("Content-type","application/json");
            InputStream input = null;

            try {
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();

                input = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(input,"UTF-8"),8);
                String line = null;

                while((line = reader.readLine()) != null){
                    result.append(line);
                }

            } finally {
                if(input != null) {
                    input.close();
                }
            }

        }catch (Exception e){
            //Log.d("RK","Download error", e);

        }
        return 0;
    }

    @Override
    protected void onCancelled(){
        httpPost.abort();
        playerService = null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        if(playerService == null){
            return;
        }

        try {
            JSONObject json = new JSONObject(result.toString());
            playerService.receiveCurrentData(json);
        }catch (Exception e){
            //Log.e("RK","JSON parse error",e);
            playerService.receiveCurrentData(null);
        }
    }
}
