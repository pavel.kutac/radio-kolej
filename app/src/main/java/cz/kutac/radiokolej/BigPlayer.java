package cz.kutac.radiokolej;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;


/**
 * Implementation of App Widget functionality.
 */
public class BigPlayer extends AppWidgetProvider {

    private String action = null,textShow = null, textArtist = null;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them



        this.updateAppWidget(context,appWidgetManager);
        /*final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {

            int appWidgetId = appWidgetIds[i];

            Intent intent = new Intent(context, PlayerService.class);
            intent.putExtra(RKNaming.INTENT_NAME,"");
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.big_player);
            views.setOnClickPendingIntent(R.id.widget_play_button,pendingIntent);


            Intent intentPlayer = new Intent(context, PlayerActivity.class);
            intentPlayer.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            PendingIntent pendingIntentPlayer = PendingIntent.getActivity(context, 0, intentPlayer, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.titleLayout,pendingIntentPlayer);

            appWidgetManager.updateAppWidget(appWidgetId,views);

        }*/
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        //Log.d("RK","onReceive widget");
        if(intent.hasExtra(RKNaming.INTENT_NAME)){
            String type = intent.getStringExtra(RKNaming.KEY_TYPE);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            textShow = context.getResources().getString(R.string.notifyTitle) + " - " + intent.getStringExtra(RKNaming.SHOW);
            textArtist = intent.getStringExtra("artist") + " - " + intent.getStringExtra("title");

            action = intent.getStringExtra(RKNaming.KEY_EVENT);
            if(! RKNaming.EVENT_PLAY.equals(action) && !RKNaming.EVENT_LOADING.equals(action)){
                textShow = context.getResources().getString(R.string.defaultTrack);
                textArtist = context.getResources().getString(R.string.defaultArtist);
            }
            this.updateAppWidget(context, appWidgetManager);
        }
    }

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager) {


        //Log.d("RK","action " + action + " / show:" + textShow + " / artist: " + textArtist);
        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.big_player);

        Intent intent = new Intent(context, PlayerService.class);
        intent.putExtra(RKNaming.INTENT_NAME,"");
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.widget_play_button,pendingIntent);

        Intent intentPlayer = new Intent(context, PlayerActivity.class);
        intentPlayer.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        PendingIntent pendingIntentPlayer = PendingIntent.getActivity(context, 0, intentPlayer, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.titleLayout,pendingIntentPlayer);


        if(RKNaming.EVENT_LOADING.equals(action)){
            views.setImageViewResource(R.id.widget_play_button, R.drawable.loading_static);
        }else if(RKNaming.EVENT_PLAY.equals(action)){
            views.setImageViewResource(R.id.widget_play_button, R.drawable.pause_button);
        }else{
            views.setImageViewResource(R.id.widget_play_button, R.drawable.play_button);
        }

        if(textShow == null || textArtist == null){
            textShow = context.getResources().getString(R.string.defaultTrack);
            textArtist = context.getResources().getString(R.string.defaultArtist);
        }

        views.setTextViewText(R.id.textShowRK,textShow);
        views.setTextViewText(R.id.textArtistTrack,textArtist);

        appWidgetManager.updateAppWidget(new ComponentName(context,BigPlayer.class),views);

    }
}


